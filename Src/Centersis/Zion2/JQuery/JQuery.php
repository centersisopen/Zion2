<?php

namespace Centersis\Zion2\JQuery;

class JQuery
{

    /**
     * Inicia uma instancia de ajax
     * @return Centersis\Zion2\JQuery\Ajax
     */
    public function ajax()
    {
        return new Ajax();
    }

}
