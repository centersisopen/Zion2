<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require '../vendor/autoload.php';

use Centersis\Zion2\Pixel\Form\Form;

echo '<pre>';

try {


    $form = new Form();

    $form->setAcao('cadastrar');

    $form->config('formManu', 'POST')
            ->setHeader('Cidadees');

    $campos[] = $form->hidden('cod')
            ->setValor($form->retornaValor('cod'));

    $campos[] = $form->texto('cidade_nome', 'Nome', true)
            ->setMaximoCaracteres(150)
            ->setValor($form->retornaValor('cidade_nome'));

    $campos[] = $form->botaoSalvarPadrao();

    $campos[] = $form->botaoDescartarPadrao();

    $form->processarForm($campos);
    
    echo $form->getFormHtml('cidade_nome');
    
    
} catch (\Exception $e) {
    echo $e->getMessage();
    echo '<br>';
    echo $e->getTraceAsString();
}